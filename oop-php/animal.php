<?php

class Animal{
    public $name,
           $legs,
           $cold_blooded;


        public function __construct($name,$legs,$cold_blooded)
        {
            $this->name = $name;
            $this->legs = $legs;
            $this->coldblooded = $cold_blooded;
        }

        public function detailPrint(){
            if(($this->coldblooded) == true):
                $hasil ="true";
            elseif(($this->coldblooded) == false) :
                $hasil = "false";
            endif;
            echo "{$this->name}<br>{$this->legs}<br>{$hasil}";
        }
}

?>