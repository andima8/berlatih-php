<?php
require 'animal.php';
require 'Frog.php';
require 'Ape.php';

$sheep = new Animal("shaun",2,false);
$sheep->detailPrint();
echo "<br>";
echo "<br>";

$sungokong = new Ape("kera sakti",2,false);
$sungokong->detailPrint();
echo "<br>";
$sungokong->yell(); // "Auooo"
echo "<br>";
echo "<br>";

$kodok = new Frog("buduk",4,true);
$kodok->detailPrint();
echo "<br>";
$kodok->jump() ; // "hop hop"
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

?>